// Lcmdump converts Greensea LCM data records to newline-delimited JSON
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"runtime"
	"time"

	"bitbucket.org/uwaploe/gss"
)

const Usage = `Usage: lcmdump [options] infile outfile

Reads Greensea LCM data records from INFILE and writes them to OUTFILE in
newline-delimited JSON format. If OUTFILE is "-", records are written to
standard output.
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	appendMode = flag.Bool("append", false,
		"If true, append to OUTFILE rather than overwrite")
)

type dataRecord struct {
	T    time.Time   `json:"time"`
	Src  string      `json:"source"`
	Data interface{} `json:"data"`
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()

	fin, err := os.Open(args[0])
	if err != nil {
		log.Fatalf("Open %q failed: %v", args[0], err)
	}

	var flags int

	if *appendMode {
		flags = os.O_APPEND | os.O_CREATE | os.O_WRONLY
	} else {
		flags = os.O_CREATE | os.O_WRONLY
	}

	var fout io.WriteCloser

	if args[1] == "-" {
		fout = os.Stdout
	} else {
		fout, err = os.OpenFile(args[1], flags, 0644)
		if err != nil {
			log.Fatalf("Open %q failed: %v", args[1], err)
		}
		defer fout.Close()
	}

	enc := json.NewEncoder(fout)
	count := 0
	for {
		ev, err := gss.ReadEvent(fin)
		if err != nil {
			log.Printf("%d records copied\n", count)
			break
		}
		val, err := ev.Decode()
		if err != nil {
			log.Printf("Cannot decode %q record: %v", ev.Channel(), err)
			continue
		}
		count++
		enc.Encode(dataRecord{
			T:    ev.Time(),
			Src:  ev.Channel(),
			Data: val,
		})
	}
}

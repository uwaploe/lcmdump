# Dump MuST LCM Archive Records

Lcmdump reads a MuST [LCM](http://lcm-proj.github.io) format data records and outputs them in [newline-delimited JSON](http://ndjson.org) (ndJSON) format. LCM is the messaging format used by the Greensea GS4 Inertial Navigation System.

## Installation

Visit the download page of this repository to obtain a compressed-tar archive of the compiled binary for your OS (Linux, Windows, or MacOS).

## Usage

The `-help` option displays a usage message

``` shellsession
$ lcmdump --help
Usage: lcmdump [options] infile outfile

Reads Greensea LCM data records from INFILE and writes them to OUTFILE in
newline-delimited JSON format.
  -append
    	If true, append to OUTFILE rather than overwrite
  -version
    	Show program version information and exit
```

Convert a file to njJSON.

``` shellsession
$ lcmdump gs4_imu-20200625-15.lcm imu.ndjson
2020/06/26 10:15:21 63217 records copied
$ head -n 2 imu.ndjson
{"time":"2020-06-25T08:17:51.299156-07:00","source":"OPENINS_IMU_STAT","data":{"TimeUnixSec":1593098271.2987058,"CountPublish":13710,"CountPublishError":0,"IdDevice":"KVH1750","Analogs":[{"Name":"temperature","Value":35},{"Name":"magnetometer_x","Value":0},{"Name":"magnetometer_y","Value":0},{"Name":"magnetometer_z","Value":0}],"AttitudeValid":false,"AttitudeRollDeg":0,"AttitudePitchDeg":0,"AttitudeHeadingDeg":0,"RotationalVelValid":true,"RotationalVelRollDegSec":0.05845334753394127,"RotationalVelPitchDegSec":0.05816219747066498,"RotationalVelHeadingDegSec":0.10262172669172287,"AccelerationValid":true,"AccelerationXMSec2":0.964249312877655,"AccelerationYMSec2":0.016067752614617348,"AccelerationZMSec2":0.25500068068504333,"Status":0}}
{"time":"2020-06-25T08:17:51.339246-07:00","source":"OPENINS_IMU_STAT","data":{"TimeUnixSec":1593098271.338686,"CountPublish":13711,"CountPublishError":0,"IdDevice":"KVH1750","Analogs":[{"Name":"temperature","Value":35},{"Name":"magnetometer_x","Value":0},{"Name":"magnetometer_y","Value":0},{"Name":"magnetometer_z","Value":0}],"AttitudeValid":false,"AttitudeRollDeg":0,"AttitudePitchDeg":0,"AttitudeHeadingDeg":0,"RotationalVelValid":true,"RotationalVelRollDegSec":0.05239936709403992,"RotationalVelPitchDegSec":0.05705077201128006,"RotationalVelHeadingDegSec":0.11427704989910126,"AccelerationValid":true,"AccelerationXMSec2":0.9642828702926636,"AccelerationYMSec2":0.01543369423598051,"AccelerationZMSec2":0.25536951422691345,"Status":0}}
$
```
